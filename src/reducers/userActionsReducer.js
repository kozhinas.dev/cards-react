import { SET_ACTIVE_ID } from '../actions/UserActions'

const intialState = {
	id: 1
}
export function setActiveId(state = intialState, action) {
	switch (action.type) {
		case SET_ACTIVE_ID:
			return { ...state, id: action.payload }
		default: 
		return state
	}
}