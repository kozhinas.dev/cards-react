import React from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import '../../assets/css/ProductItem.css';
import { ColorBall } from '../color-ball/ColorBall.jsx'
import { setActiveId } from '../../actions/UserActions'


export function ProductItem(props) { 
	const footerText = type => {
		switch (type) {
		case 'С курой':
			return <span className='footer__text'>Филе из цыплят с трюфелями в бульоне.</span>
		case 'С рыбой':
			return <span className='footer__text'>Головы щучьи с чесноком да свежайшая сёмгушка.</span>
		case 'С фуа-гра':
			return <span className='footer__text'>Печень утки разварная с артишоками.</span>
		}
	}
	const hoveredIn = (value) => {
		return true
	}
	const hoveredOut = (value) => {
		return false
	}
	const mouseQuantity = quantity => {
		if (quantity / 20 <= 1) {
			return <div className='product-item__mouse-quantity'>мышь в подарок</div>
		} 
		if (quantity / 20 >= 1 && quantity / 20 < 5) {
			return <div className='product-item__mouse-quantity'><span style={{ fontWeight: 'bold' }}>{Math.round(quantity / 20)}</span> мыши в подарок</div>
		}
		if (quantity / 20 >= 5) {
			return <div className='product-item__mouse-quantity'><span style={{ fontWeight: 'bold' }}>{Math.round(quantity / 20)}</span> мышей в подарок}</div>
		}
	}
	const { title, type, quantity, text, link, id, isActive, disabled, setActive } = props
  
	return (
		<div>
			{disabled ? <article className='product-item' id={id}>
  			  		<header className='product-item__header product-item__header_disabled' onClick={setActive} onMouseLeave={hoveredOut}>
  				  		<div className='product-item__triangle_disabled'></div>
  				  		<div className='product-item__slogan_disabled'>
  				  			<span className='slogan slogan_disabled'>{title ? title : `Сказочное заморское яство`}</span>
  				  		</div>
  			  		</header>
  			  		<main className='products-item__body_disabled'>
  			  			<h1 className='product-item__title product-item__title_disabled'>{title ? title : `Нямушка`}</h1>
  			  			<h2 className='product-item__type product-item__type_disabled'>{type}</h2>
  			  			<div className='product-item__quantity product-item__quantity_disabled'>{quantity} порций</div>
  		  				<div className='product-item__mouse-quantity product-item__mouse-quantity_disabled'>{mouseQuantity(quantity)}</div>
  		  				<ColorBall
                  className='color-ball__circle_disabled'
  			  				color={'#B3B3B3'} //#1698D9 or white, blue ...
  			  				size={80} 
  			  				text={text}
  			  				fontColor={'white'}
  			  			/>
  			  		</main>
  			  		<footer className='footer'>
  			  			<span className='footer__text footer__text_disabled'>Печалька, с {type} закончился.</span>
  			  		</footer>
  		  		</article> : <article className='product-item' id={id} onClick={setActive} onMouseEnter={hoveredIn} onMouseLeave={hoveredOut}>
  			  		<header className='product-item__header' >
  				  		<div className={isActive ? 'product-item__triangle_red' : 'product-item__triangle'}></div>
  				  		<div className={isActive ? 'product-item__slogan_red' : 'product-item__slogan'}>
  				  			<div className='slogan'>{title ? title : `Сказочное заморское яство`}</div>
  				  		</div>
  			  		</header>
  			  		<main className={isActive ? 'products-item__body_red' : 'products-item__body'}>
  			  			<h1 className='product-item__title'>{title ? title : `Нямушка`}</h1>
  			  			<h2 className='product-item__type'>{type}</h2>
  			  			<div className='product-item__quantity'><span style={{fontWeight: 'bold'}}>{quantity}</span> порций</div>
  		  				{mouseQuantity(quantity)}
  		  				<ColorBall 
  			  				className={isActive ? 'color-ball__circle_red' : 'color-ball__circle'} //#1698D9 or white, blue ...
  			  				size={80} 
  			  				text={text}
  			  				fontColor={'white'}
  			  			/>
  			  		</main>
  			  		<footer className='footer'>
  			  			{ isActive ? footerText(type) : <span className='footer__text'>Чего сидишь? Порадуй котэ, <a className='footer__link' href={link}><span className='footer__link-text'>купи</span>.</a></span> }
  			  		</footer>
  		  		</article>}	
		</div>
  	);
}

const mapDispatchToProps = dispatch => {
  return {
    setActiveId: id => dispatch(setActiveId(id))
  }
}

ProductItem.propTypes = { 
  quantity: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  isActive:PropTypes.number.isRequired,
  disabled:PropTypes.bool.isRequired,
  setActive:PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
	title: PropTypes.string.isRequired,
	type: PropTypes.string.isRequired
}

export default connect()(ProductItem);

