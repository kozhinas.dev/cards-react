import React, { Component } from 'react';
import '../../assets/css/App.css';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ProductItem } from '../../components/product-item/ProductItem.jsx';

class App extends Component {
  state = {
    isActive: 2,
    data: [
      {
        id: 1,
        title: 'Нямушка',
        type: 'С фуа-гра',
        quantity: 10,
        text: '5 кг',
        link: 'google.com',
        disabled: true
      },
      {
        id: 2,
        title: 'Нямушка',
        type: 'С рыбой',
        quantity: 10,
        text: '5 кг',
        link: 'google.com',
        disabled: false
      },
      {
        id: 3,
        title: 'Нямушка',
        type: 'С курой',
        quantity: 30,
        text: '5 кг',
        link: 'google.com',
        disabled: false
    }
    ]
  }
  onBtnClick = e => {
    const id = +e.currentTarget.id

    this.props.setActiveId(id)
      this.setState({
        isActive: id
      })

  }
  mainBody = (items) => {
    const mapped = items.map((item, index) => {
      if(this.state.isActive  === item.id && !item.disabled) {
        return <ProductItem 
          key={item.id}
          id={item.id}
          isActive={true}
          title={item.title}
          type={item.type}
          quantity={item.quantity}
          text={item.text}
          link={item.link}
          disabled={item.disabled}
        />
      } else {
        return <ProductItem 
          key={item.id}
          id={item.id}
          setActive={this.onBtnClick}
          isActive={false}
          title={item.title}
          type={item.type}
          quantity={item.quantity}
          text={item.text}
          link={item.link}
          disabled={item.disabled}
         
        />
      }
    });
    return mapped
  }
  render() {
  const { data } = this.state;
    return (
      <div className='App'>
        <div className='App-container'>
          <header className='App-header'>
            <h1 className='App-title'>Ты сегодня покормил кота?</h1>
          </header>
          <main className='products-container'>
            { this.mainBody(data) }
          </main>
        </div>
      </div>
    )
  }
}

const mapStateToProps = store => {
	return {
    isActive: store.setActiveId.id
	}
}
const mapDispatchToProps = dispatch => ({
    setActiveId: id => dispatch({type: 'SET_ACTIVE_ID'}) 
})

ProductItem.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  quantity: PropTypes.number.isRequired,        
  text: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  disabled: PropTypes.bool.isRequired
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
