import { combineReducers } from 'redux'
import { setActiveId } from './userActionsReducer'

export const rootReducer = combineReducers({
  setActiveId: setActiveId
})