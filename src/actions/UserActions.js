export const SET_ACTIVE_ID = 'SET_ACTIVE_ID'

export function setActiveId(e) {
  return {
    type: 'SET_ACTIVE_ID',
    payload: e.target.id //изменил currentTarget на target
  }
}
