import React from 'react';
import PropTypes from 'prop-types'
import '../../assets/css/ColorBall.css';
import { connect } from 'react-redux';

export function ColorBall(props) { 
	const splitText = (text) => {
		let textArray = text.split(' ')
		const mapped = textArray.map((item, index) => {
			if(index > 0) {
				return <div className='text color-ball__second-text' key={index+ 'a'}>{item}</div>
			} else {
				return <div className='text color-ball__first-text' key={index+ 'a'}>{item}</div>
			} 
		});
		return mapped
	}
	const { size, fontColor,  text, className } = props
	return (
  		<div 
  		className={className} 
  		style={{ 
			width: size,
			height: size }}>
			<div className='color-ball__text' style={{color: fontColor, display: 'flex', flexDirection: 'column', alignSelf: 'center', width: '100%', height: '100%', justifyContent: 'center', paddingTop: '10px'}}>{splitText(text)}</div>
		</div>
  	);
}

ColorBall.propTypes = {
	fontColor: PropTypes.string.isRequired,
	className: PropTypes.string.isRequired,
	size: PropTypes.number.isRequired,
	text: PropTypes.string.isRequired
}

export default connect()(ColorBall);
